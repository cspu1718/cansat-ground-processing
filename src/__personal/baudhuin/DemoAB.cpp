/*
 * DemoAB.cpp
 *
 */

#include <iostream>

#include "DemoAB.h"
#include "TestDataEmitter.h"
#include "RecordProcessor/RP_Echo.h"
#include "SerialPortListener.h"


void DemoAB::TestEmitterEchoTwice() {

	// Create chain of 2 processors.
	RP_StringEcho 	echo1(cout), echo2(cout);
	echo1.appendProcessor(echo2);

	// Create RecordSource to feed the chain.
	TestDataEmitter source(5,1000,echo1);

	// Go!
	source.run(RecordSource::allowUserInterrupt);

}

void DemoAB::SerialEchoTwice(unsigned int baudRate) {
	// Create chain of 2 processors.
	RP_StringEcho 	echo1(cout), echo2(cout);
	echo1.appendProcessor(echo2);

	// Create RecordSource to feed the chain.
	SerialPortListener source(echo1, baudRate);

	// Go!
	cout << "Echoing data from serial port..." << endl;
	source.run(RecordSource::allowUserInterrupt);

}
