/*
 * DemoAB.h
 */

#pragma once

/**
 * Some demos about the usage of RecordProcessors and RecordSources
 */
class DemoAB {
public:
	static void TestEmitterEchoTwice();
	static void SerialEchoTwice(unsigned int baudRate=115200);
};

