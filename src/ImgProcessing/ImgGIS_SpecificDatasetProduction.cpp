/*
 * ImgGIS_SpecificDatasetProduction.cpp
 *
 *  Created on: 20 avr. 2019
 *      Author: Steven
 */

#include <ImgProcessing/ImgGIS_SpecificDatasetProduction.h>
#include <ImgProcessing/GIS_RasterDataset.h>
#include <cmath>
#include <sstream>
#include "FileMgr.h"
#include "DebugUtils.h"
#include "GIS_ContourDataset.h"

static constexpr int IMG_TRANSPARENT = 0;

ImgGIS_SpecificDatasetProduction::ImgGIS_SpecificDatasetProduction(
					const RTP_ConfigDataSet theRTP_Config,
					const CameraCalibrationDataSet theCameraCalib,
					const std::string& theFolderIn,
					const std::string& theFolderOut)
: 	RTP_Config(theRTP_Config),
	cameraCalib(theCameraCalib),
	imgFolderIn(theFolderIn),
	imgFolderOut(theFolderOut),
	imageCounter(0)
{
	FileMgr::addFinalSlash(imgFolderIn);

	cout << "Generating GIS Datasets for image files" << endl
	     << "   from '" << imgFolderIn << "' " << endl
	     << "   into '" << imgFolderOut << "'..." << endl;
	if (!FileMgr::directoryExists(imgFolderIn)) {
		throw runtime_error(std::string("Error: directory '") + imgFolderIn + "' does not exist. Aborted.");
	}
	std::vector<std::string> imgFileNames;
	bool result= FileMgr::listDirectory(imgFileNames,imgFolderIn,"jpg");
	if (!result) {
		throw runtime_error(std::string("Cannot list directory '")  + imgFolderIn + "'. Aborted.");
	}
	if (imgFileNames.empty()) {
		throw runtime_error(std::string("No image file in directory '")  + imgFolderIn + "'. Aborted.");
	}

	if (!FileMgr::createDirectory(imgFolderOut, false, false)) {
		throw runtime_error(std::string("Error: could not create directory '") + imgFolderOut + "'. Aborted.");
	}
	FileMgr::addFinalSlash(imgFolderOut); // Add slash AFTER directory creation.


	// TODO: calculer les nombres ci-dessous à partir de la localisation!
	latDPM = (double)1/111238.3925;
	longDPM = (double)1/70972.32717;
	LOG_IF(DBG, DEBUG) << "Degree/m: lat:"<< setprecision(5) <<  latDPM << ", long:" << longDPM;
}

ImgGIS_SpecificDatasetProduction::~ImgGIS_SpecificDatasetProduction() {}

bool ImgGIS_SpecificDatasetProduction::doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut){
	recordOut=recordIn;

	stringstream imgFile;
	imgFile << setfill('0') << setw(8) << recordIn.timestamp;
	std::string imgFileIn(imgFolderIn);
	// Final slash has been added to folder path
	imgFileIn+=imgFile.str();
	imgFileIn+=".jpg";
    if (!FileMgr::fileExists(imgFileIn)) {
    	return true;
    }
	std::string GIS_RasterFileName(imgFolderOut);
	// Final slash has been added to folder path
	GIS_RasterFileName+=imgFile.str();
	GIS_RasterFileName+=".image";

	std::string GIS_ContourFileName(imgFolderOut);
	// Final slash has been added to folder path
	GIS_ContourFileName+=imgFile.str();
	GIS_ContourFileName+=".contour";

    cout << "Processing image file '" << imgFile.str() << "'..." << endl;
    cout << "  Target raster  DS: " << GIS_RasterFileName << endl;
    cout << "  Target contour DS: " << GIS_ContourFileName << endl;

    imageCounter++;
    generateContourDS(recordIn, GIS_ContourFileName);
	resampler(recordIn,imgFileIn, GIS_RasterFileName);
	return true;
}

void ImgGIS_SpecificDatasetProduction::doTerminate() {
	cout << "Generated specific GIS_Datasets for " << imageCounter << " images." << endl;
}

void ImgGIS_SpecificDatasetProduction::generateContourDS(
			const IsaTwoGroundRecord& recordIn,
			const std::string& GIS_ContourPath) {
	GIS_ContourDataset contourDS(GIS_ContourPath, false /* replace */);
	cout << "Created dataset '" << GIS_ContourPath << "'" << endl;

	// Do not include position
	contourDS.addContour(recordIn.imgCornerLat[0],
			recordIn.imgCornerLong[0], recordIn.imgCornerLat[1],
			recordIn.imgCornerLong[1], recordIn.imgCornerLat[3],
			recordIn.imgCornerLong[3], recordIn.imgCornerLat[2],
			recordIn.imgCornerLong[2], recordIn.timestamp,
			recordIn.altitude_Corrected);
	contourDS.saveAndSync();
}

void ImgGIS_SpecificDatasetProduction::resampler( const IsaTwoGroundRecord& record,
			string imageName, string imageResampledName){
	JPEG_Image image(imageName);
	unsigned int imNumChannels =image.getNumChannels();
	LOG_IF(DBG, DEBUG) << "Image loaded. Size: " << image.getSizeX() << "x" << image.getSizeY()
			<< " (" << image.getNumChannels() << " channels)";

	if (record.positionLocal[2]==0) {
		throw runtime_error("Error: local position Z is 0 in the record (Aborted).");
	}
	double targetResolutionInMeters = -record.positionLocal[2] / (1800.0 * sqrt(2));
	targetResolutionInMeters *= (  (double) cameraCalib.numPixelsX /(double) image.getSizeX());
	LOG_IF(DBG,DEBUG) << "Altitude (position.Z) =" << record.positionLocal[2] << "m";
	LOG_IF(DBG,DEBUG) << "Target resolution =" << targetResolutionInMeters << "m";
	double latitudeStep = targetResolutionInMeters * latDPM;
	double longitudeStep = targetResolutionInMeters * longDPM;

	array<array<double, 3>, 3> E;
	LOG_IF(DBG,DEBUG) << "Euler angles (R-Y-P): " << record.groundAHRS_Roll << "/" << record.groundAHRS_Yaw <<"/" << record.groundAHRS_Pitch;
	E[0][0] = cos(record.groundAHRS_Pitch) * cos(record.groundAHRS_Yaw);
	E[0][1] = cos(record.groundAHRS_Pitch) * sin(record.groundAHRS_Yaw);
	E[0][2] = -sin(record.groundAHRS_Pitch);
	E[1][0] = sin(record.groundAHRS_Roll) * sin(record.groundAHRS_Pitch) * cos(record.groundAHRS_Yaw) - cos(record.groundAHRS_Roll) * sin(record.groundAHRS_Yaw);
	E[1][1] = sin(record.groundAHRS_Roll) * sin(record.groundAHRS_Pitch) * sin(record.groundAHRS_Yaw) + cos(record.groundAHRS_Roll) * cos(record.groundAHRS_Yaw);
	E[1][2] = cos(record.groundAHRS_Pitch) * sin(record.groundAHRS_Roll);
	E[2][0] = cos(record.groundAHRS_Roll) * sin(record.groundAHRS_Pitch) * cos(record.groundAHRS_Yaw) + sin(record.groundAHRS_Roll) * sin(record.groundAHRS_Yaw);
	E[2][1] = cos(record.groundAHRS_Roll) * sin(record.groundAHRS_Pitch) * sin(record.groundAHRS_Yaw) - sin(record.groundAHRS_Roll) * cos(record.groundAHRS_Yaw);
	E[2][2] = cos(record.groundAHRS_Pitch) * cos(record.groundAHRS_Roll);
	if (DBG) {
		for (unsigned int i=0; i< 3; i++) {
			for (unsigned int j =0 ; j<3 ; j++) {
				cout << "E["<<i<<";"<<j<<"]=" << setprecision(5) <<  E[i][j] << endl;
			}
		}
	}

	double latMin  = min({   record.imgCornerLat[0], record.imgCornerLat[1], record.imgCornerLat[2], record.imgCornerLat[3] });
	double latMax  = max({	 record.imgCornerLat[0], record.imgCornerLat[1], record.imgCornerLat[2], record.imgCornerLat[3] });
	double longMin = min({	record.imgCornerLong[0],record.imgCornerLong[1],record.imgCornerLong[2],record.imgCornerLong[3]	});
	double longMax = max({	record.imgCornerLong[0],record.imgCornerLong[1],record.imgCornerLong[2],record.imgCornerLong[3]	});

	unsigned int imageResSizeY = (unsigned int)((latMax-latMin) / latitudeStep);
	unsigned int imageResSizeX = (unsigned int)((longMax-longMin) / longitudeStep);
	LOG_IF(DBG,DEBUG) << "  Can position (m):  ("<<record.positionLocal[0] <<";" << record.positionLocal[1] << ";" << record.positionLocal[2] << ") ";
	LOG_IF(DBG,DEBUG) << "  Latitude  (°): from " << std::setprecision(15)<< latMin  << " to " << latMax << ", step=" << latitudeStep;
	LOG_IF(DBG,DEBUG) << "  Longitude (°): from " << std::setprecision(15) << longMin << " to " << longMax << ", step=" << longitudeStep;
	LOG_IF(DBG,DEBUG) << "  Rectangle size in °: lat=" << (latMax-latMin) << ", long=" << (longMax-longMin) << ")";
	LOG_IF(DBG,DEBUG) << "  Rectangle size in m: X=" << (latMax-latMin)/latDPM << ", Y=" << (longMax-longMin)/longDPM << ")";
	LOG_IF(DBG,DEBUG) << "  Resampling will result in a size " << imageResSizeX << "x" << imageResSizeY;

	if (LimitResampledImageResolution) {
		while (min(imageResSizeX, imageResSizeY) > 2.5*max(image.getSizeX(), image.getSizeY()) ) {
			LOG_IF(DBG, DEBUG) << "Reducing image resolution ";
			latitudeStep *= 2.0;
			longitudeStep *= 2.0;
			imageResSizeY = (unsigned int)((latMax-latMin) / latitudeStep);
			imageResSizeX = (unsigned int)((longMax-longMin) / longitudeStep);
			LOG_IF(DBG,DEBUG) << "Resampling reduced: " << imageResSizeX << "x" << imageResSizeY;
		}
	}

	if ((imageResSizeX == 0) || (imageResSizeY ==0)) {
		throw runtime_error("Resampled image size is 0 in either direction. Aborted).");
	}
	if ((imageResSizeX < image.getSizeX()) || (imageResSizeY < image.getSizeY())) {
		cout << "*** Warning: resampled image results in lower resolution" << endl;
	}

	size_t bufferSize = (imageResSizeX*imageResSizeY*imNumChannels)*sizeof(unsigned char);
	LOG_IF(DBG,DEBUG) << "Allocating bitmap buffer (" << bufferSize << " bytes).... " ;
	unsigned char *bitmapRes = (unsigned char *) malloc(bufferSize);
	if(bitmapRes == nullptr){
		throw runtime_error("*** Error: cannot allocate bitmap.");
	}
	unsigned char *redRes = nullptr;
	unsigned char *greenRes = nullptr;
	unsigned char *blueRes = nullptr;

	cout << "Reprojecting..." << endl;
	for(unsigned long y = 0; y < imageResSizeY; y++){
		if (y%100 == 0) {
			cout << '.' << flush;
		}
		for(unsigned long x = 0; x < imageResSizeX; x++){
			double currentLat = latMax - y*latitudeStep;
			double currentLong = longMin + x*longitudeStep;
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<   "Current Lat/long=" << currentLat << "/" << currentLong;

			//cartesian
			array<double, 3> pixel =  {
					(currentLat - RTP_Config.localReferentialOrigin_Latitude) / latDPM,
					(currentLong - RTP_Config.localReferentialOrigin_Longitude) / longDPM,
					0.0};
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<  "Reprojecting pixel (in NED, origin at launch site) ("<<x <<";" << y <<") = , in m (" << pixel[0]<<";"<<pixel[1]<<";" << pixel[2] << ")";

			//NED
			for(unsigned long i = 0; i < 3; i++){
				pixel[i] -= record.positionLocal[i];
			}
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<  "  Can position (m):  ("<<record.positionLocal[0] <<";" << record.positionLocal[1] << ";" << record.positionLocal[2] << ") ";
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<  "  Direction vector, in NED (origin on can position): ("<<pixel[0]<<";"<<pixel[1]<<";" << pixel[2] << ")";

			//imu
			array<double, 3> tempPixels = {0, 0, 0};
			tempPixels = pixel;
			pixel = {0, 0, 0};

			for (unsigned long i = 0; i < 3; i++){
				for (unsigned long j = 0; j <3; j++){
					pixel[i] += E[i][j] * tempPixels[j];
				}
			}
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<  "  After rotation matrix: ("<<pixel[0]<<";"<<pixel[1]<<";" << pixel[2] << ")";

			double n = cameraCalib.f / pixel[2];
			pixel[0] *= n;
			pixel[1] *= n;
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<  "  In IMU referential: ("<<pixel[0]<<";"<<pixel[1]<<";" << pixel[2] << ")";

			//camera
			double tempPixel = pixel[0];
			pixel[0] = pixel[1];
			pixel[1] = -tempPixel;
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<  "  In Camera referential: ("<<pixel[0]<<";"<<pixel[1]<<";" << pixel[2] << ")";

			//centered image
			pixel[0] /= cameraCalib.pixelSizeX;
			pixel[1] /= cameraCalib.pixelSizeY;
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<  "  In centered image referential: ("<<pixel[0]<<";"<<pixel[1]<<";" << pixel[2] << ")";

			//image
			pixel[0] += (double)cameraCalib.numPixelsX/2;
			pixel[1] += (double)cameraCalib.numPixelsY/2;
			LOG_IF(DBG_ALL_PIXELS,DEBUG) <<  "  In image referential: ("<<pixel[0]<<";"<<pixel[1]<<";" << pixel[2] << ")";

			// Defining the value of the pixel (x;y)
			size_t pixelPositionInBitmap= y*imNumChannels*imageResSizeX+x*imNumChannels;
			redRes   = &bitmapRes[ pixelPositionInBitmap];
			greenRes = &bitmapRes[ pixelPositionInBitmap+1];
			blueRes  = &bitmapRes[ pixelPositionInBitmap+2];
			if (pixelPositionInBitmap >= bufferSize) {
				stringstream msg;
				msg << "*** Error: accessing pixel outside resampled image buffer. " << endl
					<< "    Pixel (" << x << ";" << y <<"), bufferSize=" << bufferSize
					<< "    Pixel position in buffer: "	<< pixelPositionInBitmap << " to " << pixelPositionInBitmap+2;
				throw runtime_error(msg.str());
			}

			if(pixel[0] < cameraCalib.numPixelsX && pixel[0] >= 0 && pixel[1] < cameraCalib.numPixelsY && pixel[1] >= 0){
				unsigned char *red, *green, *blue, *red1, *green1, *blue1,*red2, *green2, *blue2,*red3, *green3, *blue3;
				getColor((unsigned int)(floor(pixel[0])),(unsigned int)(floor(pixel[1])),red,green,blue,image);
				getColor((unsigned int)(floor(pixel[0])),(unsigned int)(ceil(pixel[1])), red1,green1,blue1,image);
				getColor((unsigned int)(ceil(pixel[0])) ,(unsigned int)(floor(pixel[1])),red2,green2,blue2,image);
				getColor((unsigned int)(ceil(pixel[0])) ,(unsigned int)(ceil(pixel[1])), red3,green3,blue3,image);
				redRes[0]   = (unsigned char) (((int) red[0]+ (int)red1[0]+ (int)red2[0]+(int)red3[0])/4);
				greenRes[0] = (unsigned char) (((int) green[0]+ (int) green1[0]+(int) green2[0]+(int) green3[0])/4);
				blueRes[0]  = (unsigned char) (((int) blue[0]+ (int) blue1[0]+ (int) blue2[0]+ (int) blue3[0])/4);
			}
			else{
				redRes[0]  =IMG_TRANSPARENT;
				greenRes[0]=IMG_TRANSPARENT;
				blueRes[0] =IMG_TRANSPARENT;
			}
		}
		//cout<<";"<<endl;
	}
    cout << endl << "  Done reprojecting. Generating GIS dataset..." << endl;
	// Generate raster dataset
	GIS_RasterDataset resampledImage(	imageResampledName+".jpg", bitmapRes,
										imageResSizeX, imageResSizeY,
										imNumChannels, true,
										longMin, latMax,
										longitudeStep,
										latitudeStep);

	//Destroy
	if(bitmapRes){
		free(bitmapRes);
		bitmapRes=NULL;
	}
}

void ImgGIS_SpecificDatasetProduction::getColor(const unsigned int x, const unsigned int y, unsigned char *&red,unsigned char *&green, unsigned char *&blue, JPEG_Image &image){
	unsigned char *bitmap =image.getBitmap();
	unsigned int imNumChannels =image.getNumChannels();

	red   = &bitmap[ y*imNumChannels*image.getSizeX()+x*imNumChannels];
	green = &bitmap[(y*imNumChannels*image.getSizeX()+x*imNumChannels)+1];
	blue  = &bitmap[(y*imNumChannels*image.getSizeX()+x*imNumChannels)+2];
}

