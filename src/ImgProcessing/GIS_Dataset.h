/*
 * GIS_Dataset.h
 *
 */

#pragma once
#include <string>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#include <gdal_priv.h>
#pragma GCC diagnostic pop


/** @ingroup ImgProcessing
 *  @brief An abstract class to handle the production of a GIS dataset containing layers for the
 *  various data.
 *  The class encapsulates the creation of the dataset, insertion of one layer per image.
 *  and the creation of multiple datasets to avoid very large number of layers
 *  This class makes use of the GDAL library which is assumed to be installed
 *  (if not install from www.gdal.org, see instructions in folder 7900).
 *
 *  The underlying GDAL library is only initialized once, and several instances of any subclass of
 *  GIS_Dataset can be used simultaneously without problem.
 *
 *  Usage
 *  =====
 *  @code
 *  	GIS_RasterDataset ds(fileName, true, 10);
 *  	(...)
 *  	// use subclass-specific methods to fill dataset.
 *		(...)
 * 	 	ds.saveAndClose()
 * 	 	// Optional: will be saved to file by the destructor anyway.
 * 	 	ds.saveAndSync();
 *  @endcode
 *  To delete an existing dataset (or sequence of datasets):
 *  @code
 *  	GIS_ContourDataset::delete(fileName, true);
 *  	// Remark on Windows: see documentation of delete() method.
 *  @endcode
 *
 *  If the GDAL library is used outside this class and already initialized, be sure to set the optional parameters
 *  initGDAL to false in the constructor and the delete() method to avoid re-initialization.
 */
class GIS_Dataset {
public:
	/**
	 *  Create a dataset (or a first dataset, if more layers are inserted than the
	 *  maximum number for a single dataset).
	 *  @param theFileName The name of the dataset (depending on the format, it can be
	 *  				a file or a folder name). Can be a relative or absolute path,
	 *  				but in this case the containing folder is expected to exist.
	 *  @param doReplaceExisting If true, an existing file or directory named "fileName"
	 *  				is deleted (if any). If false, and the file or directory
	 *  				exists, it is open, and appended.
	 *  @param theMaxNumberOfLayers The maximum number of layers allowed in the dataset.
	 *  				If this number is exceeded, the dataset is closed and a new one
	 *  				is opened, named fileName$ where fileName is the original
	 *  				fileName used to create this object and $ is a sequence number,
	 *  				starting with 2.
	 *  				If 0, the number of layers is not limited.
	 *  @param theFormat The format for storing the dataset (chosen among the formats
	 *  			    supported by GDAL for creation of multiple raster layers
	 *  			    (see https://gdal.org/ogr_formats.html)
	 *  @param GDAL_DCAP_capability The capability string to check (as defined by the
	 *         			various GDAL_DCAP_xxxx constants. GDAL_DCAP_CREATE is already
	 *         			checked by the superclass.
	 *  @param initGDAL If true, the GDAL library is initialized the first time any subclass of
	 *  				GIS_Dataset is created (default). Set this
	 *  				to false if GDAL was initialized prior to using subclasses of GIS_Dataset.
	 */
	GIS_Dataset(		const std::string theFileName,
						bool doReplaceExisting,
						unsigned int theMaxNumberOfLayers,
						const std::string theFormat,
						const char* GDAL_DCAP_capability,
						bool initGDAL);

	/** Destructor. Dataset is saved to file before the object is deleted */
	virtual ~GIS_Dataset();

	/** Obtain the number of layers already in dataset
	 *  @return the number of layers.
	 */
	virtual unsigned int getLayerCount();
	/** @brief Save the dataset to file. Called automatically by the destructor */
	virtual void saveAndSync();

	/** Obtain the maximum number of layers allowed in the dataset.
	 *  @return the maximum nuber of layers allowed, or 0 if unlimited.
	 */
	virtual unsigned int getMaxNumberOfLayers() const { return maxNumberOfLayers; } ;

	/** Check every element expected in the dataset is indeed present.
	 *  Problems are reported on cout.
	 *  @param  verbose If true, diagnostic is printed on cout, even when no problem
	 *  		is detected.
	 *  @return true if everything ok, false otherwise.
	 */
	virtual bool checkIntegrity(bool verbose=false)=0;

	/** Get the size of the raster in the X direction (only relevant for raster datasets).
	 *  @return The size (in pixels) in the X direction.
	 */
	virtual unsigned int getSizeX() const {return sizeX; };

	/** Get the size of the raster in the Y direction (only relevant for raster datasets).
	 *  @return The size (in pixels) in the Y direction.
	 */
	virtual unsigned int getSizeY() const {return sizeY; };

	/** Get the number of bands (only relevant for raster datasets).
	 *  @return The number of bands (assumed to be 1 byte each).
	 */
	virtual unsigned int getNumBands() const {return numBands; };

protected:
	/** Delete all dataset files from disk (be sure not to call this function while files
	 *  are in use!
	 *  @bug For unknown reasons, on Windows only, deletion of the dataset sometimes
	 *  succeed but leaves files on the disk.
	 *  @param theFileName The name of the dataset (depending on the format, it can be
	 *  				a file or a folder name). Can be a relative or absolute path,
	 *  				but in this case the containing folder is expected to exist.
	 *  @param includeNextDatasets If true, files from any dataset that could have been
	 *  				created as successors of the provided one (when the number of
	 *  				contours / dataset was exceeded) are deletes as well.
	 *  @param theFormat The format for storing the dataset (chosen among the formats
	 *  			    supported by GDAL for creation of multiple vector layers
	 *  			    (see https://gdal.org/ogr_formats.html).
	 *  @param initGDAL If true, the GDAL library is initialized (default). Set this
	 *  				to false if GDAL was initialized prior to constructing this
	 *  				object.
	 */
	static bool deleteFiles(	const std::string theFileName,
								bool includeNextDatasets,
								const std::string theFormat,
								bool initGDAL);

	/** Close current dataset if any, and open a new one with next sequence number */
	virtual void openNewDataset();

	/** Check the underlying GDAL driver has the required capability. Throws a runtime
	 *  error if the capability is not supported.
	 *  GDAL_DCAP_CREATE is already checked by the superclass.
	 *  @param GDAL_DCAP_capability The capability string to check (as defined by the
	 *         various GDAL_DCAP_xxxx constants.
	 */
	virtual void checkDriverCapability(const char *GDAL_DCAP_capability);

	/** Create file name including the sequence number
	 *  @param sequenceNbr The sequence number to integrate in the file name */
	virtual std::string getFileNameWithSequence(unsigned short sequenceNbr);
	/** Create file name including the sequence number
	 *  @param theFileName The file name to consider.
	 *  @param sequenceNbr The sequence number to integrate in the file name */
	static std::string getFileNameWithSequence(std::string theFileName, unsigned short sequenceNbr);

	/** Set the data type of the dataset (call before calling openNewFile() */
	virtual void setDataType(GDALDataType type) { dataType=type; };

	/** Check a GDAL return code. If different from OGRERR_NONE, a runtime_error
	 *  is thrown, including msg and the error code.
	 *  @param msg The human-readable message to include in the exception, in case of error.
	 *  @param error The return code to check.
	 */
	virtual void checkGDAL_Error(const std::string &msg, OGRErr error);

	/** Create and initialize a EPSG:4326 SRS */
	virtual OGRSpatialReference *getEPSG4326_SRS();

	/** Obtain the sequence number (from 1 to ...) of the currently active
	 *  part of the dataset.
	 * @return sequence number (first part is 1)
	 */
	virtual unsigned short getSequenceNbr(){ return sequenceNbr; };

	GDALDataset *dataset;			/**< Pointer to dataset (if any), or null (otherwise) */
	unsigned int sizeX;				/**< Number of pixels on X direction (for raster datasets */
	unsigned int sizeY;				/**< Number of pixels on Y direction (for raster datasets */
	unsigned int numBands;			/**< Number of bands (for raster datasets */
private:
	static bool GDAL_Initialized;	/**< True if the GDAL library is initialized already */
	bool replaceExisting;			/**< True if existing dataset must be removed when
										 attempting to create a new one (otherwise an error
										 is triggered */
	std::string fileName;			/**< File name (possibly with relative or absolute path
										 for first dataset */
	unsigned int maxNumberOfLayers; /**< Maximum number of layers in a dataset.
										 If exceeded, a new dataset is created, with incremented
										 sequence number. */
	std::string format;				/**< Dataset format used */
	unsigned short sequenceNbr;		/**< Sequence number of current dataset. */
	GDALDataType dataType;			/**< The data type (only relevant for raster data) */
	GDALDriver *gdalDriver;			/**< Pointer to driver used to manage the dataset */
};

