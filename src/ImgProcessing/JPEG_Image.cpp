/*
 * JPEG_Image.cpp
 *
 *  Created on: 05 Feb 2019
 *      Author: Alain
 */

#include <ImgProcessing/JPEG_Image.h>
#include <stdio.h>
#include <sstream>
#include <jpeglib.h>
#include <jerror.h>
#include <iostream>
#include <cassert>

using namespace std;

JPEG_Image::JPEG_Image() :  bitmap(nullptr), sizeX(0), sizeY(0), numChannels(0) {}

JPEG_Image::JPEG_Image(const string& fileName) : JPEG_Image()
{
	loadImage(fileName);
}

void JPEG_Image::loadImage(const string& fileName) {
	stringstream msg;
	unsigned long dataSize;     // length of the file
	unsigned char * rowptr[1];    // pointer to an array
	struct jpeg_decompress_struct info; //for our jpeg info
	struct jpeg_error_mgr err;          //the error handler

	if (bitmap) {
		free(bitmap);
		bitmap= nullptr;
	}
	FILE* file = fopen(fileName.c_str(), "rb");  //open the file
	info.err = jpeg_std_error(& err);
	jpeg_create_decompress(& info);   //fills info structure

	//if the jpeg file doesn't load
	if(!file) {
		msg.clear();
		msg << "Error reading JPEG file: '" << fileName << "'";
		throw runtime_error(msg.str());
	}

	jpeg_stdio_src(&info, file);
	jpeg_read_header(&info, TRUE);   // read jpeg file header
	jpeg_start_decompress(&info);    // decompress the file

	//set width and height
	sizeX = info.output_width;
	sizeY = info.output_height;
	numChannels = (unsigned int) info.num_components;
	//type = GL_RGB;
	//if(channels == 4) type = GL_RGBA; // NEEDED ?

	dataSize = sizeX * sizeY * numChannels; // OR 4 if RGBA ?

	//--------------------------------------------
	// read scanlines one at a time & put bytes
	//    in jdata[] array. Assumes an RGB image
	//--------------------------------------------
	bitmap = (unsigned char *) malloc(dataSize);
	if (!bitmap) {
		msg.clear();
		msg << "Cannot allocate bitmap ("<< dataSize << " bytes) for '" << fileName << "'" << endl;
		throw runtime_error(msg.str());
	}
	while (info.output_scanline < info.output_height) // loop
	{
		// Enable jpeg_read_scanlines() to fill our bitmap array
		rowptr[0] = (unsigned char *)bitmap +  // secret to method
				numChannels* info.output_width * info.output_scanline;
		jpeg_read_scanlines(&info, rowptr, 1);
	}
	//---------------------------------------------------

	jpeg_finish_decompress(&info);   //finish decompressing

	jpeg_destroy_decompress(&info);
	fclose(file);                    //close the file
}

JPEG_Image::~JPEG_Image() {
	if (bitmap) free(bitmap);
}

unsigned char * JPEG_Image::getBitmap() const {
	if (bitmap) {
		return bitmap;
	}
	else {
		throw runtime_error("JPEG_Image::getBitmap(): bitmap is null");
	}
}

unsigned int JPEG_Image::getSizeX() const {
	if (bitmap) {
		return sizeX;
	}
	else {
		throw runtime_error("JPEG_Image::sizeX(): bitmap is null");
	}
}

unsigned int JPEG_Image::getSizeY() const {
	if (bitmap) {
		return sizeY;
	}
	else {
		throw runtime_error("JPEG_Image::sizeY(): bitmap is null");
	}
}

unsigned int JPEG_Image::getNumChannels() const {
	if (bitmap) {
		return numChannels;
	}
	else {
		throw runtime_error("JPEG_Image::getNumChannels(): bitmap is null");
	}
}

void JPEG_Image::addTestRGB_Blocks(unsigned int blockLength, unsigned int blockHeight)
{
	assert(bitmap);
	// FOR TEST: modify bitmap to add red, blue and green blocks
	cout << "******** Altering bitmap for test *********" << endl;
	for (unsigned int line= 0; line < blockHeight; line ++) {
		unsigned char *startLine =bitmap+numChannels*line*sizeX;
		for (unsigned int i=0; i<blockLength ; i++) {
			unsigned char *red   = startLine+(i*numChannels);
			unsigned char *green = startLine+((i+blockLength)*numChannels);
			unsigned char *blue  = startLine+((i+2*blockLength)*numChannels);
			*(red)=255; *(red+1)=0; *(red+2)=0;
			*(green)=0; *(green+1)=255; *(green+2)=0;
			*(blue)=0; *(blue+1)=0; *(blue+2)=255;
		}
	}
}

