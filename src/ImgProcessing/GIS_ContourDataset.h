/*
 * GIS_ContourDataset.h
 *
 */

#pragma once
#include <string>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#include <gdal_priv.h>
#pragma GCC diagnostic pop

#include "ImgProcessing/GIS_Dataset.h"


/** @ingroup ImgProcessing
 *  @brief A class to handle the production of a GIS dataset containing layers for the various image contours.
 *  The class encapsulates the creation of the dataset, insertion of one layer per image contour
 *  and the creation of multiple datasets to avoid very large number of layers
 *  This class makes use of the GDAL library which is assumed to be installed
 *  (if not install from www.gdal.org, see instructions in folder 7900).
 *
 *  The underlying GDAL library is only initialized once, and several instances of this
 *  class can be used simultaneously without problem.
 *
 *  Fields
 *  ------
 *  Each contour has the following fields (in addition to its geometry):
 *  - timestamp (number of milliseconds since CanSat power-up)
 *  - altitude (relative to ground in meters)
 *
 *  About styles
 *  ------------
 *  The use of feature styles (as described in https://www.gdal.org/ogr_feature_style.html)
 *  has been considered, tested and abandoned for the following reasons:
 *  *Saving style info in a dataset seems ok in the following formats: DXF, PDF,
 *   MapInfo File, DGN, KML, JML.
 *  *Retrieving style info from the data set while in memory is ok in any format, but after
 *   saving to file and reopening the dataset, we never managed to retrieve the style strings
 *   or style tables.
 *  *Default brush style applied with adding layers in QGIS is quite adequate, and labels
 *   should not be set on every contour anyway, it would clutter the display.
 *  *As a consequence, styles will be applied manually on a selected subset of the layers
 *   using the QGIS interface.
 *
 *  Usage
 *  -----
 *  @code
 *  	GIS_ContourDataset ds(fileName, true, 10);
 *  	(...)
 *  	//(repeat any number of times, to generate a layer/contour):
 *		ds.addLayer(lat1, long1, lat2, long2, lat3, long3, lat4, long4, canLat, canLong);
 *		//(repeat any number of times, to build the trajectory layer):
 *		(...)
 * 	 	ds.saveAndClose()
 * 	 	// Optional: will be saved to file by the destructor anyway.
 * 	 	ds.saveAndSync();
 *  @endcode
 *  To delete an existing dataset (or sequence of datasets):
 *  @code
 *  	GIS_ContourDataset::delete(fileName, true);
 *  	// Remark on Windows!
 *  @endcode
 *
 *  If the GDAL library is used outside this class and already initialized, be sure to set the optional parameters
 *  initGDAL to false in the constructor and the delete() method to avoid re-initialization.
 */
class GIS_ContourDataset : public GIS_Dataset {
public:
	/**
	 *  Create a dataset (or a first dataset, if more layers are inserted than the
	 *  maximum number for a single dataset.
	 *  @param theFileName The name of the dataset (depending on the format, it can be
	 *  				a file or a folder name). Can be a relative or absolute path,
	 *  				but in this case the containing folder is expected to exist.
	 *  @param doReplaceExisting If true, an existing file or directory named "fileName"
	 *  				is deleted (if any). If false, and the file or directory
	 *  				exists, it is open, and appended.
	 *  @param theMaxNumberOfLayers The maximum number of layers allowed in the dataset.
	 *  				If this number is exceeded, the dataset is closed and a new one
	 *  				is opened, named fileName$ where fileName is the original
	 *  				fileName used to create this object and $ is a sequence number,
	 *  				starting with 2.
	 *  				If 0, the number of layers is not limited.
	 *  @param theFormat The format for storing the dataset (chosen among the formats
	 *  			    supported by GDAL for creation of multiple vector layers
	 *  			    (see https://gdal.org/ogr_formats.html).
	 *  @param initGDAL If true, the GDAL library is initialized the first time any subclass of
	 *  				GIS_Dataset is created (default). Set this
	 *  				to false if GDAL was initialized prior to using subclasses of GIS_Dataset.
	 */
	GIS_ContourDataset(	const std::string theFileName,
						bool doReplaceExisting=true,
						unsigned int theMaxNumberOfLayers=0,
						const std::string theFormat="ESRI Shapefile",
						bool initGDAL=true);

	/** Destructor. Dataset is saved to file before the object is deleted */
	virtual ~GIS_ContourDataset();

	/** @brief Insert a contour in the dataset. A contour is a closed line with four points
	 *  @param lat1 Latitude of first point (decimal degrees)
	 *  @param long1 Longitude of first point (decimal degrees)
	 *  @param lat2 Latitude of second point (decimal degrees)
	 *  @param long2 Longitude of second point (decimal degrees)
	 *  @param lat3 Latitude of third point (decimal degrees)
	 *  @param long3 Longitude of third point (decimal degrees)
	 *  @param lat4 Latitude of fourth point (decimal degrees)
	 *  @param long4 Longitude of fourth point (decimal degrees)
	 *  @param timestamp The number of milliseconds since CanSat power-up at the time of the image capture
	 *  @param altitudeFromGround The altitude relative to the ground at which the image was captured.
	 *  @param latitude The latitude of the can (any value >=500= no data)
	 *  @param longitude The longitude of the can (any value >=500= no data)*
	 */
	virtual void addContour(	double lat1, double long1,
								double lat2, double long2,
								double lat3, double long3,
								double lat4, double long4,
								unsigned long timestamp,
								double altitudeFromGround,
								double canLatitude=1000,
								double canLongitude=1000);

	/** Obtain the number of contours already in dataset
	 *  @return the number of contours.
	 */
	virtual unsigned int getContourCount();

	/** Check every element expected in the dataset is indeed present.
	 *  Problems are reported on cout.
	 *  @param  verbose If true, diagnostic is printed on cout, even when no problem
	 *  		is detected.
	 *  @return true if everything ok, false otherwise.
	 */
	virtual bool checkIntegrity(bool verbose=false);

	/** Delete all dataset files from disk (be sure not to call this function while files
	 *  are in use!
	 *  @bug For unknown reasons, on Windows only, deletion of the dataset sometimes
	 *  succeed but leaves files on the disk.
	 *  @param theFileName The name of the dataset (depending on the format, it can be
	 *  				a file or a folder name). Can be a relative or absolute path,
	 *  				but in this case the containing folder is expected to exist.
	 *  @param includeNextDatasets If true, files from any dataset that could have been
	 *  				created as successors of the provided one (when the number of
	 *  				contours / dataset was exceeded) are deletes as well.
	 *  @param theFormat The format for storing the dataset (chosen among the formats
	 *  			    supported by GDAL for creation of multiple vector layers
	 *  			    (see https://gdal.org/ogr_formats.html).
	 *  @param initGDAL If true, the GDAL library is initialized (default). Set this
	 *  				to false if GDAL was initialized prior to constructing this
	 *  				object.
	 */
	static bool deleteFiles(	const std::string theFileName,
								bool includeNextDatasets,
								const std::string theFormat="ESRI Shapefile",
								bool initGDAL=true);
protected:
	/** Create a field in a layer
	 * @param layer The layer to create a field in
	 * @param fieldName	The name of the field to create
	 * @param fieldType The type of the field to create (OFTString, OFTReal etc... )
	 */
	virtual void createField(OGRLayer* layer, const char* fieldName,  OGRFieldType fieldType);

	/** Create a viewing line feature to denote the can position corresponding to a
	 *  contour
	 *  @param layer The layer to create the viewing line in
	 *  @param canLongitude The longitude of the can
	 *  @param canLatitude The latitude of the can
	 *  @param cornerLongitude The longitude of the viewed image corner.
	 *  @param cornerLatitude The latitude of the viewed image corner.
	 *
	 */
	virtual void addViewingLine(	OGRLayer* layer,
									double canLongitude, double canLatitude,
									double cornerLongitude, double cornerLatitude);

	/** Check every element expected in the dataset is indeed present.
	 *  Problems are reported on cout.
	 *  @param layer The layer to check
	 *  @param  verbose If true, diagnostic is printed on cout, even when no problem
	 *  		is detected.
	 *  @return true if everything ok, false otherwise.
	 */
	virtual bool checkContourLayerIntegrity(OGRLayer* layer, bool verbose=false);
	void addViewingLine(double canLongitude, double canLatitude, double long2,
			double lat2, std::stringstream msg, OGRLayer* newLayer);
};

