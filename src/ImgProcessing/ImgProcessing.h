/*
 * ImgProcessing.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the package should include a tag @ingroup ImgProcessing
 * in the class documentation block.
 */

 /** @defgroup ImgProcessing Img Processing
 *  @brief The set of ground processing classes dedicated to Image Processing.
 *  
 *  The Img Processing package contains all classes used in the Ground Segment to process images.
 *  
 *  _Dependencies_\n
 *  This package relies on:
 *  	- Generic Data Abstraction Layer (GDAL) library
 *  	- libjpeg
 *  	- the easylogging++ library
 *
 *  @todo //.
 *  
 *  
 *  _History_\n
 *  The library was created by the 2018-2019 Cansat team (IsaTwo).
 */
