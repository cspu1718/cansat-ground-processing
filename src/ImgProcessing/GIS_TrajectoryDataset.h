/*
 * GIS_TrajectoryDataset.h
 *
 */

#pragma once
#include "ImgProcessing/GIS_Dataset.h"

#include <string>
#include <ogr_geometry.h>

/** @ingroup ImgProcessing
 *  @brief A class to handle the production of a GIS dataset containing one layer for the can trajectory
 *  The class encapsulates the creation of the dataset and the insertion successive can positions
 *  This class makes use of the GDAL library which is assumed to be installed
 *  (if not install from www.gdal.org, see instructions in folder 7900).
 *
 *  The underlying GDAL library is only initialized once, and several instances of any subclass of
 *  GIS_Dataset can be used simultaneously without problem.
 *
 *  Fields
 *  ------
 *  None.
 *
 *  About styles
 *  ============
 *  See note in class #GIS_ContourDataset.
 *
 *  Usage
 *  =====
 *  @code
 *  	GIS_TrajectoryDataset ds(fileName, true);
 *  	(...)
 *  	//(repeat any number of times, to generate a layer/contour):
 *		ds.addCanPosition(lat, long);
 *		(...)
 * 	 	ds.saveAndClose()
 * 	 	// Optional: will be saved to file by the destructor anyway.
 * 	 	// Once saved, positions may not be added anymore.
 * 	 	ds.saveAndSync();
 *  @endcode
 *  To delete an existing dataset (or sequence of datasets):
 *  @code
 *  	GIS_TrajectoryDataset::delete(fileName, true);
 *  	// Remark on Windows!
 *  @endcode
 *
 *  If the GDAL library is used outside this class and already initialized, be sure to set the optional parameters
 *  initGDAL to false in the constructor and the delete() method to avoid re-initialization.
 */
class GIS_TrajectoryDataset  : public GIS_Dataset {
public:
	/**
	 *  Create a dataset with a single layer for displaying the Cansat trajectory.
	 *  @param theFileName The name of the dataset (depending on the format, it can be
	 *  				a file or a folder name). Can be a relative or absolute path,
	 *  				but in this case the containing folder is expected to exist.
	 *  @param doReplaceExisting If true, an existing file or directory named "fileName"
	 *  				is deleted (if any). If false, and the file or directory
	 *  				exists, it is open, and appended.
	 *  @param theFormat The format for storing the dataset (chosen among the formats
	 *  			    supported by GDAL for creation of multiple vector layers
	 *  			    (see https://gdal.org/ogr_formats.html).
	 *  @param initGDAL If true, the GDAL library is initialized the first time any subclass of
	 *  				GIS_Dataset is created (default). Set this
	 *  				to false if GDAL was initialized prior to using subclasses of GIS_Dataset.
	 */
	GIS_TrajectoryDataset(const std::string theFileName,
			bool doReplaceExisting=true,
			const std::string theFormat="ESRI Shapefile",
			bool initGDAL=true);

	virtual ~GIS_TrajectoryDataset();

	/** Add a can position to the trajectory depicted in the trajectory
	 *  layer (as a polyline).
	 *  @param latitude The latitude of the can
	 *  @param longitude The longitude of the can.
	 */
	virtual void addCanPosition(double latitude, double longitude);

	/** @brief Save the dataset to file. Called automatically by the destructor */
	virtual void saveAndSync();

	/** Delete all dataset files from disk (be sure not to call this function while files
	 *  are in use!
	 *  @bug For unknown reasons, on Windows only, deletion of the dataset sometimes
	 *  succeed but leaves files on the disk.
	 *  @param theFileName The name of the dataset (depending on the format, it can be
	 *  				a file or a folder name). Can be a relative or absolute path,
	 *  				but in this case the containing folder is expected to exist.
	 *  @param theFormat The format for storing the dataset (chosen among the formats
	 *  			    supported by GDAL for creation of multiple vector layers
	 *  			    (see https://gdal.org/ogr_formats.html).
	 *  @param initGDAL If true, the GDAL library is initialized (default). Set this
	 *  				to false if GDAL was initialized prior to constructing this
	 *  				object.
	 */
	static bool deleteFiles(	const std::string theFileName,
			const std::string theFormat="ESRI Shapefile",
			bool initGDAL=true);

	/** Check every element expected in the dataset is indeed present.
	 *  Problems are reported on cout.
	 *  @param  verbose If true, diagnostic is printed on cout, even when no problem
	 *  		is detected.
	 *  @return true if everything ok, false otherwise.
	 */
	virtual bool checkIntegrity(bool verbose=false);
protected:
	virtual bool checkTrajectoryLayerIntegrity(OGRLayer* layer, bool verbose=false);
private:
	OGRLineString	trajectory;  /**< The polyline depicting the trajectory */
	bool creationOver;			 /**< If true, the polyline was saved to file and can not be updated anymore */
};

