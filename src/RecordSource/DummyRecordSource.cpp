/*
 * DummyRecordSource.cpp
 */

#include "DummyRecordSource.h"
#include "RecordProcessor/RecordProcessor.h"
#include "DebugUtils.h"

static constexpr auto DBG=false;

DummyRecordSource::DummyRecordSource(
			RecordSource::visualFeedback_t feedback,
			unsigned long thePeriod)
   : RecordSource(feedback), period(thePeriod) {
	DBG_FCT
	cout << "BBB (TMP)" << endl << flush;

	record.clear();
	record.timestamp=35000;
}

DummyRecordSource::~DummyRecordSource() {
	// TODO Auto-generated destructor stub
}

bool DummyRecordSource::feedOneRecordToProcessor(
		Processor& processor,
		bool& recordProcessed,
		bool& success) {
	DBG_FCT
	auto ts=record.timestamp + period;
	record.clear();
	record.timestamp=ts;
	stringstream s;
	record.printCSV(s);
	processor.processString(s.str());
	success=true;
	recordProcessed=true;
	return true;
}
