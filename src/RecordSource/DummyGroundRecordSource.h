/*
 * DummyGroundRecordSource.h
 */

#pragma once
#include "RecordSource.h"
#include "IsaTwoGroundRecord.h"

/** @ingroup RecordSource
 *  @brief A RecordSource which generates random IsaTwoGroundRecords, indefinetely.
 *  Operation can be interrupted by pressing return, as for any
 *  processor.
 *  Intended use: feed the RT-GUI.
 *  NB: Use a RP_Delay processor to have the records
 *  processed at a realistic rate (this class justy emits records
 *  as fast as possible).
 *  Used for test only. */
class DummyGroundRecordSource : public RecordSource {
public:
	DummyGroundRecordSource(RecordSource::visualFeedback_t feedback, unsigned long period);
	virtual ~DummyGroundRecordSource();
	/** Feed the processor with one random IsaTwoRecord.
	 *  @param  processor  The processor to feed.
	 *  @param	recordProcessed True if a record was found and processed. In this case, the result of the
	 *  		processing is in parameter success.
	 *  @param  success This parameter is updated to true by the subclass if the processing of
	 *  	            the record was successful, and to false otherwise. The value is irrelevant
	 *  	            if recordProcessed is false.
	 *  @return true if the method should be called again (because there is data left to process)
	 *          false if the last record has been processed or some error make it impossible
	 *          to process additional records.
	 */
	virtual bool feedOneRecordToProcessor(Processor &processor, bool& recordProcessed, bool &success);
protected:
	typedef enum {
		roll, yaw, pitch, all
	} AnglePhase_t;

	IsaTwoGroundRecord record;
	unsigned long period; /**< The generation period, in msec. */

	// Parameters for data
	AnglePhase_t anglePhase;
	double deltaRoll, deltaYaw, deltaPitch;
	double deltaTempBMP;
	double deltaTempCorrected;
	double deltaAltitude, deltaPressure;
	double deltaLat, deltaLong;
	double deltaLatCorners, deltaLongCorners;
	double deltaPosX, deltaPosY, deltaPosZ;
	double deltaCO2;

	/** Change provided value by delta. If lower- or upperbound is exceeded, change sign of delta.
	 *  @return true if delta's sign was changed, false otherwise.
	 */
	virtual bool changeValue(double &value, double &delta, double lowerBound, double upperBound);
};

