/*
 * RecordSource.cpp
 */

#include "RecordSource/RecordSource.h"
#include "DebugUtils.h"

static constexpr auto DBG=false;

RecordSource::RecordSource(Processor& theProcessor, visualFeedback_t visualFeedback):
				processor(&theProcessor),
				visualFeedback(visualFeedback==withVisualFeedback),
				cancellationWatch(nullptr){	DBG_FCT	};

RecordSource::RecordSource(visualFeedback_t visualFeedback):
		processor(nullptr),
		visualFeedback(visualFeedback==withVisualFeedback),
		cancellationWatch(nullptr){	DBG_FCT	};

RecordSource::~RecordSource(){
		DBG_FCT
		if (cancellationWatch) delete cancellationWatch;
	};

uint32_t RecordSource::feedProcessor(allowUserInterrupt_t userInterrupt) {
	DBG_FCT
	bool success=false;
	bool done=false;
	bool recordProcessed;
	uint32_t count=0;
	uint32_t errorCount=0;
	assert(processor != nullptr);

	if (userInterrupt && (!cancellationWatch)) cancellationWatch=new CancellationWatch();
	while (!cancellationWatch->cancelled() && !done) {
		LOG_IF(DBG,DEBUG) << "RP::feed_processor: In loop";
		done=!feedOneRecordToProcessor(*processor, recordProcessed, success);
		if (recordProcessed) {
			if (success) {
				count++;
			}
			else {
				errorCount++;
				std::cout << "*** Error processing record! *** " << std::endl;
			}
			if (visualFeedback && ((count % 100L)==0L)) {
				std::cout << "." << std::flush;
			}
		}
	}
	if (userInterrupt && (cancellationWatch)) {
		delete cancellationWatch;
		cancellationWatch=nullptr;
	}
	// Do not worry about exceptions: the destructor will stop it anyway.
	std::cout << std::endl << "Successfully processed " << count  << " records ("
			  << errorCount << " errors)."<< std::endl;
	return count;
}

uint32_t RecordSource::run(allowUserInterrupt_t userInterrupt){
  	DBG_FCT
		if (processor == nullptr) {
			throw runtime_error("*** RecordSource::run() called without a processor");
		}

  	uint32_t  num=0;
  	if (prepare()) {
  		num = feedProcessor(userInterrupt);
  		processor->terminateProcessing();
  		terminate();
  	} else {
  		std::cout << "Error during preparation of record source (aborted)." << std::endl;
  	}
  	return num;
  }

void RecordSource::deleteProcessorChain() {
	if (processor) {
		processor->deleteAppendedProcessors();
		delete processor;
		processor=nullptr;
	}
}

void RecordSource::printProcessorChainDescription(ostream& os) const {
	os << "RecordSource: " << typeid(*this).name() << ": ";
	if (processor) {
		os << endl;
		processor->printProcessorChainDescription(os);
	} else os << "No processor attached." << endl;
}
