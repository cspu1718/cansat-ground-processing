/*
 * CameraCalibrationDataSet.cpp
 */

#include <CameraCalibrationDataSet.h>
#include <iostream>
#include "FileMgr.h"
#include "DebugUtils.h"
#include "CSV_Row.h"

static constexpr bool DBG=false;

using namespace std;

CameraCalibrationDataSet::CameraCalibrationDataSet() {
	numPixelsX=numPixelsY=0;
	angle1TBD = angle2TBD = angle3TBD = 0.0;
	viewingAngleX = viewingAngleY=0.0;
}

CameraCalibrationDataSet::CameraCalibrationDataSet(const std::string& dataFile) {
	if (!FileMgr::isRegularFile(dataFile)) {
		throw runtime_error(string("Inexistant Camera calibration file '")+dataFile+"'");
	}
	DBG_FCT
	try {
		ifstream in;
		ostringstream msg;
		in.open(dataFile);
		if (!in.good()) {
			msg.clear();
			msg << "Cannot open file '"<<dataFile <<"'.";
			throw runtime_error(msg.str());
		}
		LOG_IF(DBG, DEBUG) << "Reading camera calibration file '" << dataFile <<"'....";
		CSV_Row row(in);
		row.next(true);

		size_t idx=0;
		numPixelsX= row.getUInt(idx++);
		numPixelsY=row.getUInt(idx++);
		pixelSizeX=row.getDouble(idx++);
		pixelSizeY=row.getDouble(idx++);
		angle1TBD=row.getDouble(idx++);
		angle2TBD=row.getDouble(idx++);
		angle3TBD=row.getDouble(idx++);
		viewingAngleX=row.getDouble(idx++);
		viewingAngleY=row.getDouble(idx++);
		f=row.getDouble(idx++);

		LOG_IF(DBG, DEBUG) << "Camera calibration file processed. ";
	}
	catch(exception &e) {
		cout << "Exception while camera calibration file: '" <<dataFile << "'" << endl;
		cout << e.what() << endl;
		throw;
	}
}
