/*
 * RP_CSV_FileWriter_Filtered.h
 *
 *  Created on: 20 juin 2019
 *      Author: Steven et Bryan
 */

#pragma once
#include <RecordProcessor/RecordProcessor.h>
#include "RP_CSV_FileWriter.h"
#include "IsaTwoGroundRecord.h"

template<class RECORD>
class RP_CSV_FileWriter_Filtered: RP_CSV_FileWriter<IsaTwoGroundRecord> {
public:
	//RP_CSV_FileWriter_Filtered();
	//virtual ~RP_CSV_FileWriter_Filtered();
protected:
	/** Just echo the provided record on the ostream provided in the constructor.
	 *  @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 *  @param recordOut The record resulting from the process: it is set to recordIn.
	 *  @return Always true.
	 */
	bool doProcess(const RECORD& recordIn, RECORD& recordOut) {
		recordOut=recordIn;
		if((recordIn.timestamp - ts) >= 210){
		os << std::endl << recordIn;
		ts = recordIn.timestamp;
		}
		return true;
	};

	/** Just echo the provided record on the ostream provided in the constructor.
	 *  @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 *  @return Always true.
	 */
	bool doProcess(const std::string& recordIn) {
		os << std::endl << recordIn;
		return true;
	};
private:
	unsigned long ts = 0;
};
