/*
 * RP_Transparent.h
 */

#pragma once
#include <IsaTwoGroundRecord.h>
#include "RecordProcessor.h"

/** @ingroup RecordProcessor
 *  @brief A record processor which does just nothing. Used as a placeholder
 *  during development.
 */
template<class RECORD_IN, class RECORD_OUT>
class RP_Transparent : public RecordProcessor<RECORD_IN, RECORD_OUT> {
public:
	RP_Transparent()
		: RecordProcessor<RECORD_IN, RECORD_OUT>(Processor::noVisualFeedback) {};

	virtual ~RP_Transparent() {};

	/** Just copy in to out. (RECORD_IN must have an assignment operator using a RECORD_OUT).
	 * @param recordIn  The record to process.
	 * @param recordOut The record resulting from the processing.
	 * @return Always true (cannot possibly fail).
	 */
	virtual bool doProcess(const RECORD_IN& recordIn, RECORD_OUT& recordOut) {
		recordOut=recordIn;
		return true;
	}
};


