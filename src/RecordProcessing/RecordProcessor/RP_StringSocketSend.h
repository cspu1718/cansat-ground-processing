/*
 * RP_StringSocketSend.h
 *
 */

#pragma once
#include "SocketStringSender.h"
#include "RecordProcessor/StringProcessor.h"
#include "DebugUtils.h"
/** @ingroup RecordProcessor
 *  @brief A string processor which manages a TCP connexion and sends data into a TCP socket.
 *
 *  This class relies on the SocketStringSender gateway to run a socket server, accept a single
 *  external connection and sends everything to the socket as long as the client is connected.
 *  If no connection is open, or the client disconnects, it either:
 *   - blocks until another connection request is received, blocking any activity in this thread
 *     (if option=suspendOnClientDisconnection)
 *   - just ignores the incoming record (just copying them to is input) until a connexion is
 *     reestablished.
 *
 */
class RP_StringSocketSend : public StringProcessor, public SocketStringSender {
public:
	using StringProcessor::doProcess;
	// The above line brings all doProcess methods in this name space. This is
	// required to avoid name-hiding.
	/** Constructor
	 *  @param thePortNbr The number of the port on which to accept TCP connections.
	 *  @param option If suspendOnClientDeconnexion, the processing is suspended
	 *  		 	  when the client is not connected and resumed afterward.
	 *  			  This is the appropriate mode when processing offline
	 *  			  data (it would not make sense not to wait for a client to ingest them).
	 *  			  If proceedDuringClientDisconnection, incoming records are just passed to the
	 * 				  next processor (if any) when no client is connected.
	 * 				  This is the appropriate mode when processing a real-time feed: it would
	 *  			  not make sense to miss data because the client disconnected.
	 *  @param fb	 If withVisualFeedback, the processor prints some feedback on cout:
	 *  				a '.' for each record transferred,
	 *  				a '!' for each record ignored because no client is connected and
	 *  				      suspendOnClientDeconnexion is false.
	 */
	RP_StringSocketSend(unsigned short thePortNbr,
						ClientDisconnectOption_t option=suspendOnClientDisconnection,
						Processor::VisualFeedback_t fb=Processor::noVisualFeedback)
			: StringProcessor(fb),
			  SocketStringSender(thePortNbr, option)
			  {
				DBG_FCT
			  }

protected:
	   /** Prepare the socket sender. It is called just before the first call to
	    *  of the doProcess() methods.
	    *  @return true if preparation was successful and the processor is ready
	    *          to accept records, false otherwise.
	    */
	   	virtual bool doPrepare() { DBG_FCT ; return prepare(); };

	   	/** Process the provided string.
	   	 *  @param in The string to process.
	   	 *  @param out The string resulting from the processing (always identical to in).
	   	 * @return true if processing was successful, false otherwise.
	   	 */
	   	virtual bool doProcess(const std::string& in , std::string out) {
	   		out=in;
	   		return send(in);
	   	};

	   	/** Process the provided record, received as a String, generating result as string as well.
	   	 * This method is overloaded to avoid parsing the string into a CSV_Record: this is not
	   	 * required.
	   	 * @param recordIn  The record to process, in string format. It is assumed NOT to be terminated by
	   	 *                  a '\\n'.
	   	 * @param recordOut The data resulting from the processing, in string format, without a final '\\n'.
	   	 * 					Always set to recordIn
	   	 * @return true if processing was successful, false otherwise.
	   	 */
	   	virtual bool doProcess(const std::string& recordIn, std::string& recordOut) {
	   		recordOut=recordIn;
	   		return send(recordIn);
	   	}

	   	/** Overload this method to perform any action required as soon as possible after the last
	   	 *  record is processed and should preferably not wait until the destructor is called (optional)
	   	 *  This method will be called at the latest in the destructor, but a well behaved user could
	   	 *  have it called earlier (RecordSource does). This method MUST support multiple calls.
	   	 */
	   	virtual void doTerminate() { terminate();};
private:
	static constexpr auto DBG=false;
};




