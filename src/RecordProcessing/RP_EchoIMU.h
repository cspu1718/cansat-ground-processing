/*
 * RP_EchoIMU.h
 *
 */

#pragma once

#include <RecordProcessor/RecordProcessor.h>
#include "IsaTwoGroundRecord.h"
#include <iostream>

/**  @ingroup RecordProcessing
 *  @brief A processor to echo the IMU data on a stream and transparently forward
 *  input data.
 */
class RP_EchoIMU: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord> {
public:
	/** Constructor
	 *  @param outputStream The ostream on which record must be echoed
	 */
	RP_EchoIMU(std::ostream &outputStream) : RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>(), os(outputStream) {};

	/** Just echo part of IMU data on the output stream provided in the constructor.
	 * @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 * @param recordOut The record resulting from the process: it is set to recordIn.
	 * @return Always true.
	 */
	bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut) {
		os << std::fixed << std::setprecision(numDecimals)
		<<"Raw:A:"
		   << "X="  <<  std::setw(numDecimals+3) << recordIn.accelRaw[0]
		   << ", Y="  <<  std::setw(numDecimals+3) << recordIn.accelRaw[1]
		   << ", Z="  <<  std::setw(numDecimals+3) << recordIn.accelRaw[2]
		   <<";G:"
		   << "X="  <<  std::setw(numDecimals+3) << recordIn.gyroRaw[0]
		   << ", Y="  <<  std::setw(numDecimals+3) << recordIn.gyroRaw[1]
		   << ", Z="  <<  std::setw(numDecimals+3) << recordIn.gyroRaw[2]
		   <<";M:"
		   << "X="  <<  std::setw(numDecimals+3) << recordIn.magRaw[0]
		   << ", Y="  <<  std::setw(numDecimals+3) << recordIn.magRaw[1]
		   << ", Z="  <<  std::setw(numDecimals+3) << recordIn.magRaw[2]//<< std::endl
		<<" Cal:A:"
		   << "X="  <<  std::setw(numDecimals+3) << recordIn.calibratedAccel[0]
		   << ", Y="  <<  std::setw(numDecimals+3) << recordIn.calibratedAccel[1]
		   << ", Z="  <<  std::setw(numDecimals+3) << recordIn.calibratedAccel[2]
		   <<";G:"
		   << "X="  <<  std::setw(numDecimals+3) << recordIn.calibratedGyro[0]
		   << ", Y="  <<  std::setw(numDecimals+3) << recordIn.calibratedGyro[1]
		   << ", Z="  <<  std::setw(numDecimals+3) << recordIn.calibratedGyro[2]
		   <<";M:"
		   << "X="  <<  std::setw(numDecimals+3) << recordIn.calibratedMag[0]
		   << ", Y="  <<  std::setw(numDecimals+3) << recordIn.calibratedMag[1]
		   << ",Z="  <<  std::setw(numDecimals+3) << recordIn.calibratedMag[2]<< std::endl;
		recordOut=recordIn;
		return true;};

private:
	std::ostream& os;
	static constexpr unsigned short numDecimals=3;
};

