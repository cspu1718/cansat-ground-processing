/*
 * IMU_Calibrator.h
 *
 *  Created on: 17 dec. 2018
 *      Author: Steven
 */

#ifndef RECORDPROCESSING_IMU_CALIBRATOR_H_
#define RECORDPROCESSING_IMU_CALIBRATOR_H_

#include <IMU_CalibrationDataSet.h>
#include <IsaTwoGroundRecord.h>
#include "RecordProcessor/RecordProcessor.h"
//#define DBG_IMU

//#undef CHECK_PROCESSOR_OUTPUT_RANGE


/** @ingroup RecordProcessing
 *  @brief A processor which calibrates data from the IMU (accelerometers, magnetometers and gyroscopes).
 */
class IMU_Calibrator: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>{
public:
	IMU_Calibrator(const IMU_CalibrationDataSet theIMU_Calib)
		: IMU_Calib(theIMU_Calib){};

	virtual ~IMU_Calibrator() {};
	/** Perform a IMU calculation and store it using the base class'
		 *  storeResult() method.
		 * @param recordIn  The record to process.
		 * @param recordOut The record resulting from the processing.
		 * @return True if the process success.
		 */
	virtual bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut) {
			recordOut=recordIn;

			//Magnetometer calibration
			double uncalibrated_values[3] = {0, 0, 0};
			for(unsigned long i = 0; i<3; i++){
				uncalibrated_values[i] = recordIn.magRaw[i] - IMU_Calib.magOffset[i];
			}

			double resultMag[3];
			for(unsigned long i = 0; i<3; i++){
				for(unsigned long j = 0; j<3; j++){
					resultMag[i] += IMU_Calib.magTransformationMatrix[i][j] * uncalibrated_values[j];
				}
			}


			//Accelerometer and gyroscope calibration
			double resultAccel[3],resultGyro[3];
			for(unsigned long i = 0; i < 3; i++){
				resultAccel[i] = (recordIn.accelRaw[i] - IMU_Calib.accelOffset[i]) * IMU_Calib.accelResolution[i];
				resultGyro[i] = (recordIn.gyroRaw[i] - IMU_Calib.gyroOffset[i]) * IMU_Calib.gyroResolution[i];
			}

			//Debug
#ifdef DBG_IMU
			cout<<endl<<endl;
			for(unsigned long i = 0; i<3; i++){
				cout<<uncalibrated_values[i]<<"="<<recordIn.magRaw[i]<<"-"<<IMU_Calib.magOffset[i];
				cout<<endl;
			}
			cout<<endl;
			for(unsigned long i = 0; i<3; i++){
				for(unsigned long j = 0; j<3; j++){
					cout<<IMU_Calib.magTransformationMatrix[i][j]<<"*"<<uncalibrated_values[j]<<"+";
				}
				cout<<"="<<resultMag[i];
			}
			cout<<endl<<endl;
#endif

			//Compliant result
#ifdef CHECK_PROCESSOR_OUTPUT_RANGE
			for(unsigned long i = 0; i < 3; i++){
				if(abs(resultMag[i]) >= 1500){
					throw runtime_error("ERROR : VALUES FOR THE CALIBRATED MAG IMPOSSIBLE !!!");
				}
				if(abs(resultAccel[i]) >= 1000){
					throw runtime_error("ERROR : VALUES FOR THE CALIBRATED ACCEL IMPOSSIBLE !!!");
				}
				if(abs(resultGyro[i]) >= 3000){
					throw runtime_error("ERROR : VALUES FOR THE CALIBRATED GYRO IMPOSSIBLE !!!");
				}
			}
#endif

			//Storage
			bool ok = true;
			for(unsigned long i = 0; i < 3; i++){
				ok =ok && storeResult(resultMag[i], recordOut.calibratedMag[i],"mag");
				ok =ok && storeResult(resultAccel[i], recordOut.calibratedAccel[i],"accel");
				ok =ok && storeResult(resultGyro[i], recordOut.calibratedGyro[i],"gyro");
			}
			return ok;
		}

private:
	IMU_CalibrationDataSet IMU_Calib;
};

#endif /* RECORDPROCESSING_IMU_CALIBRATOR_H_ */
