/*
 * IMU_CalibrationDataSet.h
 */

#pragma once
#include <array>
#include <string>

/** @ingroup RecordProcessing
 *  @brief A class containing all calibration data for the Inertial Management Unit. */
class IMU_CalibrationDataSet {
public:
	/** Create a data set initialized with 0 values */
	IMU_CalibrationDataSet();
	/** Create a data set from calibration file
	 *  @param dataFile The path to the file to load.
	 */
	IMU_CalibrationDataSet(const std::string& dataFile);

	std::array< std::array<double, 3>,3> magTransformationMatrix;
	std::array<double,3> magOffset;
	std::array<double,3> accelResolution;
	std::array<double,3> accelOffset;
	std::array<double,3> gyroResolution;
	std::array<double,3> gyroOffset;
};

