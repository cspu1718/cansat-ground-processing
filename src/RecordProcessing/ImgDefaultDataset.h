/*
 * ImgDefaultDataset.h
 *
 */

#pragma once

#include <RecordProcessor/RecordProcessor.h>
#include "IsaTwoGroundRecord.h"
#include <iostream>
#include "GIS_TrajectoryDataset.h"
#include "GIS_ContourDataset.h"

/**  @ingroup RecordProcessing
 *  @brief A processor to generate GIS trajectory and contour datasets from IsaTwoGroundRecords.
 */
class ImgDefaultDataset: public RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord> {
public:
	/** Constructor
	 *  @param theOutputDir The path to the directory in which GIS dataset are
	 *  produced.
	 *  @param theGIS_DatasetBaseName The basename to be used for GIS datasets.
	 *  @param theSubsamplingFactor GIS Datasets are produced using 1 record every subsamplingFactor
	 *  	   (skipping records without a fix).
	 *  @param includeCanPosition If true, include the position of the can in the
	 *         coutour dataset, as well as the viewing directions.
	 */
	ImgDefaultDataset(	const std::string& theOutputDir,
						const std::string& theGIS_DatasetBaseName,
						unsigned int theSubsamplingFactor,
						bool includeCanPosition)
	: RecordProcessor<IsaTwoGroundRecord, IsaTwoGroundRecord>(),
	  outputDir(theOutputDir),
	  subsamplingFactor(theSubsamplingFactor),
	  boardResetWithinInputData(false),
	  positionCounter(0L),
	  contourCounter(0L),
	  previousTimestamp(0),
	  trajectoryDS(NULL),
	  contourDS(NULL),
	  includePosition(includeCanPosition)
  {
		if (subsamplingFactor==0) {
			throw runtime_error(std::string("Error: subsampling factor is 0. Aborted."));
		}
		if (!FileMgr::createDirectory(outputDir, false, false)) {
			throw runtime_error(std::string("Error: could not create directory '") + outputDir + "'. Aborted.");
		}
		std::string commonDS_Path(outputDir);
		FileMgr::addFinalSlash(commonDS_Path);
		commonDS_Path+=theGIS_DatasetBaseName;
		std::string dsPath(commonDS_Path);
		dsPath+=".trajectory";
		trajectoryDS = new GIS_TrajectoryDataset(dsPath,false /*replace*/);
		cout << "Created dataset '" << dsPath << "'" << endl;
		
		dsPath=commonDS_Path;
		dsPath+=".contour";
		contourDS=  new GIS_ContourDataset(dsPath, false /* replace */,50);
		cout << "Created dataset '" << dsPath << "'" << endl;
		if (includePosition) {
			cout << "Can position and viewing directions are included." << endl;
		}
		else {
			cout << "Can position and viewing directions are NOT included." << endl;
		}
		cout << "Subsampling factor = " << subsamplingFactor << endl;
  };

  virtual ~ImgDefaultDataset() {
	  if (trajectoryDS) {
		  delete trajectoryDS;
		  trajectoryDS=nullptr;
	  }
	  if (contourDS) {
		  delete contourDS;
		  contourDS=nullptr;
	  }
  }
protected:
	/** Just echo part of the provided record on the output stream provided in the constructor.
	 *  (only data relevant to the RT GUI.
	 * @param recordIn  The record to process. It is assumed NOT to be terminated by a '\\n'.
	 * @param recordOut The record resulting from the process: it is set to recordIn.
	 * @return Always true.
	 */
	bool doProcess(const IsaTwoGroundRecord& recordIn, IsaTwoGroundRecord& recordOut) {
		static unsigned int subsamplingCounter=0;
		recordOut=recordIn;
		if (!boardResetWithinInputData && (recordIn.timestamp < previousTimestamp)) {
			// This means that the master board was reset and the data was catenated
			// in a single file. Mapping should happen within a single run.
			// If this condition is detected: stop mapping !
			boardResetWithinInputData=true;
			cout << endl
					<< "*** Error: smaller timestamp detected (now " << recordIn.timestamp
					<< ", previously " << previousTimestamp << endl;
			cout << "*** Dataset generation interrupted. " << endl;
			return false;
		}
		if (boardResetWithinInputData) {
			return true;  // Avoid reporting an error on every next record.
		}
		previousTimestamp=recordIn.timestamp;

		// skip records according to subsampling factor.
		if (subsamplingCounter < subsamplingFactor) {
			subsamplingCounter++;
			LOG_IF(DBG_SUBSAMPLING, DEBUG) << "Skipped record at " << recordIn.timestamp << " ("
					<< subsamplingCounter << "/" << subsamplingFactor << ") ";
			return true;
		}

		// try to process the record
		// skip if no fix
		if (!recordIn.GPS_Measures) {
			LOG_IF(DBG, DEBUG) << "Skipped record at " << recordIn.timestamp
					<< " (no fix)";
			return true;
		}

		// Complete trajectory dataset
		trajectoryDS->addCanPosition(recordIn.GPS_LongitudeDegrees, recordIn.GPS_LatitudeDegrees);
		cout << "Added position at " << recordIn.timestamp << endl;
		positionCounter++;

		// skip contour if angles too big
		if (	(recordIn.groundAHRS_Roll > AngleThresholdForContour) ||
				(recordIn.groundAHRS_Pitch > AngleThresholdForContour))
		{
			LOG_IF(DBG, DEBUG) << "Skipped record at " << recordIn.timestamp
					<< " (Roll or Pitch angle above threshold)";
			return true;

		}
		// Generate Contour.
		double canLat=recordIn.GPS_LatitudeDegrees;
		double canLong=recordIn.GPS_LongitudeDegrees;
		if (!includePosition) {
			// If lat or long >500 it is ignored by the dataset.
			canLat+=1000;
			canLong+=1000;

		}
		contourDS->addContour(	recordIn.imgCornerLat[0], recordIn.imgCornerLong[0],
								recordIn.imgCornerLat[1], recordIn.imgCornerLong[1],
								recordIn.imgCornerLat[3], recordIn.imgCornerLong[3],
								recordIn.imgCornerLat[2], recordIn.imgCornerLong[2],
								recordIn.timestamp,
								recordIn.altitude_Corrected,
								canLat,
								canLong);
		contourCounter++;
		subsamplingCounter=0;
		return true;
	};

	void doTerminate() {
		if (boardResetWithinInputData) {
			cout << "*** Error: detected a clock reset within the input file." << endl;
			cout << "***        Dataset generation was interrupted and is probably wrong." <<endl;
			cout << "***        Split the data file and run again using the right part." << endl;
		}
		else {
			trajectoryDS->saveAndSync();
			contourDS->saveAndSync();
			cout << "Generated " << contourCounter << " contour layers and 1 trajectory layer with "
				 << positionCounter << " positions." << endl;
		}
	}
private:
	static constexpr bool DBG_SUBSAMPLING=false;
	static constexpr double AngleThresholdForContour=M_PI/3;
	static constexpr bool DBG=false;
	 std::string outputDir; /**< The path to the directory in which GIS dataset are produced. */
	 unsigned int subsamplingFactor; /**< GIS Datasets are produced using 1 record every
	 	 	 	 	 	 	 	 	 	 subsamplingFactor records (skipping records without a GPS fix) */
	bool boardResetWithinInputData; /**< Did we detect a timestamp smaller than a previous one ? */
	unsigned long positionCounter;	/**< Number of contour layers generated. */
	unsigned long contourCounter;	/**< Number of contour layers generated. */
	unsigned long previousTimestamp; /**< The timestamp of the previous record processed (0 = none). */
	GIS_TrajectoryDataset* trajectoryDS; /**< The trajectory dataset created from the records */
	GIS_ContourDataset* contourDS; /**< The contour dataset created from the records. */
	bool includePosition;	       /**< If true, include the position of the can in the
	 *         coutour dataset, as well as the viewing directions.  */
};

