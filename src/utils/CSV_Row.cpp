/*
 * CSV_Row.cpp
 *
 * See header file comment
 *
 */

#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
#include <assert.h>
#include "CSV_Row.h"
#include "TrimStdString.h"
#include "DebugUtils.h"
constexpr auto DBG=false;
constexpr auto DBG_STREAM_STATE=false;

using namespace std;

CSV_Row::CSV_Row() : stream(nullptr){};

bool CSV_Row::readNextLineFromStream(bool skipHeader)
{
	/* While getline(stream,buffer) will set the eofbit if it reaches the end of a file,
	 * it will also set the failbit, since the desired characters (a line) couldn't be
	 * extracted. This will happen if the last line is empty, for instance. If exceptions
	 * are activated on the stream, this will result in an exception.
	 * Since we want to support exception-enables streams as well as non-exception enables
	 * streams, the logic is implemented, stream->good() is tested AND the whole thing is
	 * wrapped in a try-catch */
	DBG_FCT
	assert(stream != nullptr);

	try {
		LOG_IF(DBG,DEBUG)  << "CSV_Row: a) stream->good()=" <<  stream->good();
		if (!stream->good()) return false;
		if (skipHeader) currentLineNumber+=skipOneRow(*stream);
		if (!stream->good()) return false;

		// We skip empty lines and lines starting with '#' (comments)
		bool keepReading=true;
		bool validRow=false;
		while (keepReading) {
			currentRow=""; // clear previous.
			std::getline(*stream, currentRow);
			currentLineNumber++;
			trim(currentRow);
			LOG_IF(DBG,DEBUG) << "CSV_Row: while: currentRow='" << currentRow << "'" ;

			// Is the row valid ?
			validRow= ((currentRow.size() > 0) && (currentRow[0] != '#'));
			if (validRow) {
				keepReading=false;
				LOG_IF(DBG,DEBUG)  << "CSV_Row: while: row is good!" ;
			} else {
				if (stream->good()) {
					keepReading=true;
				}
				else {
					LOG_IF(DBG,DEBUG)  << "CSV_Row: while: EOF" ;
					keepReading=false;  // exit the loop.
				}
			} // else validRow
		} // while
		return validRow;
	} catch(...) {
		// Could not read a line. Swallow exception.
		return false;
	}
}

bool CSV_Row::next(bool skipHeader){
	DBG_FCT
	assert(stream != nullptr);
	if (!readNextLineFromStream(skipHeader)) {
		return false;
	}

	// At this stage, we have a valid string to process.
	LOG_IF(DBG,DEBUG) << "Processing row '" << currentRow << "'";

	std::stringstream   lineStream(currentRow);
	std::string         cell;

	m_data.clear();
	while(std::getline(lineStream, cell, ','))
	{
		m_data.push_back(cell);
	}
	// This checks for a trailing comma with no data after it.
	if (!lineStream && cell.empty())
	{
		// If there was a trailing comma then add an empty element.
		m_data.push_back("");
	}
	LOG_IF(DBG_STREAM_STATE,DEBUG) << "Current row: " << currentRow << endl;
	LOG_IF(DBG_STREAM_STATE,DEBUG) << "Stream state: eof=" << stream->eof()
			<< ", good=" << stream->good();
	return true;
}

unsigned int CSV_Row::skipOneRow(istream &in) {
	string row="";
	unsigned int count=0;
	while ((row[0] == '#') || (row.size() ==0))	{
		std::getline(in, row);
		trim(row);
		count++;
	}
	return count;
}

double CSV_Row::getDoubleFromNextLine() {
	DBG_FCT
	next();
	return getDouble(0);
}

bool CSV_Row::getDoubleArrayFromNextLine(array<double, 3> &arr) {
	DBG_FCT
	bool found=next();
	for (unsigned long i=0; i<3;i++) arr[i]=getDouble(i);
	return found;
}

std::string const& CSV_Row::operator[](std::size_t index) const  {
	if(index >= m_data.size()) {
		std::ostringstream msg;
		msg << "CSV_Row: attempt to read element with index=" << index
				<< " while only " << m_data.size() << " elements are available.";
		msg << std::endl << "Current row: '" << currentRow << "'";
		throw runtime_error(msg.str());
	}
	/* Using at() rather than operator[] to have bound checking. Do not use
       assertions here: we don't want the program to stop if a corrupt CSV line
       is read
	 */
	return m_data.at(index);
}

