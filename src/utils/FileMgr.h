/*
 * FileMgr.h
 *
 * Source:  http://insanecoding.blogspot.com/2007/11/pathmax-simply-isnt.html
 * 			http://insanecoding.blogspot.com/2007/11/implementing-realpath-in-c.html
 * Completed with a couple of methods.
 */

#include <string>
#include <vector>
#include <sys/stat.h>

/** @ingroup Utilities
 * @brief A basic collection of cross-platform static methods to perform operations on
 * files and directories (see limitations below).
 *
 * @remark Know limitations
 * Although some functions have many refinements on Linux-MacOS (to handle remote file
 * systems, symbolic and hard links, raw devices and more...), not all of them have,
 * and only the basic operations (no permission issues, no links, no special files...)
 * have been tested.
 * Windows support is only provided with the same restrictions.
 *  */
class FileMgr {
public:
	static bool getCwd(std::string& path);
	static bool symlinkResolve(const std::string& start, std::string& end);
	static bool realPath(const std::string& path, std::string& resolved_path, bool resolve_link = true);
	static bool deleteFile(	const std::string& pathToFile,
							bool silentIfInexistent=true);
	/** @warning Ugly, non portable Unix only version.*/
	static bool deleteFiles(	const std::string& pathToDir,
								const std::string& fileNameWithWildcards,
								bool echoOnCout=false);
	/** @warning Ugly, non portable Unix only version. Files are overwritten if existing in destinationDir */
	static bool copyFiles(	const std::string& pathToDir,
								const std::string& fileNameWithWildcards,
								const std::string& destinationDir,
								bool echoOnCout=false);
	/** Move a file to another directory.
	 *  Current limitation: this method does not support move across file systems (will fail).
	 *  @param pathToFile Relative or absolute path to file to move.
	 *  @param destinationDir The destination directory. If not existent, it is created.
	 *  @param silentlyOverwrite If true and destination directory already contains a file with same name, it
	 *  						 is silently overwritten, otherwise the operation fails.
	 *  @param interactive If true, feedback is provided on cout and user interaction is requested on failure.
	 *  @return true if everything ok, false if anything happened (cannot create directory, file with same
	 *          name already in directory etc.)
	 */
	static bool moveFile(	const std::string& pathToFile,
							const std::string& destinationDir,
							bool silentlyOverwrite,
							bool interactive);
	/** Copy a file
	 *  @param originalPath Relative or absolute path to file to copy.
	 *  @param destinationPath The destination for the copy. It must be in an existent directory and may not exist.
	 *  @return true if everything ok, false if anything happened (cannot create directory, file with same
	 *          name already in directory etc.)
	 */
    static bool copyFile(	const std::string& originalPath,
    						const std::string& destinationPath);

	/*! Test whether a path is an existing directory
	 *  @return true if the path exists and is a directory.
	 */
	static bool directoryExists(	const std::string& dirPath);

	/*! Test whether a path exists and is a file.
	 *  On Linux, path exists if file is either regular or a link
	 *  On Windows, path exists only if file is regular (see comment in code).
	 *  @return true if the path exists and is a file.
	 */
	static bool fileExists(	const std::string& filePath);

	/*! Test whether a path is a regular file
	 *  @return true if the path exists and is a regular file.
	 */
	static bool isRegularFile(const std::string &filePath);
	/** Define whether the provided file name has the provided extension
	 *  @param path The path to the file (absolute or relative) or the file name.
	 *  @param extension The extension to look for (with or without the initial dot).
	 *  @return true if the path ends with a dot followed by the extension.
	 */
	static bool hasExtension(const std::string &path, const std::string& extension);
	/** Get the part of the path after a dot ('.') in the file name.
	 *  @param path The path to the file (absolute or relative) or the file name.
	 *  @param lastExtensionOnly If true only the part after the last dot in the file name
	 *  			is returned. Otherwise the part after the first dot is returned
	 *  @return the extension
	 */
	static std::string getExtension(const std::string &path, bool lastExtensionOnly=true);

	static bool replaceExtension(	const std::string &original,
									const std::string &originalExt,
									const std::string &newExt,
									std::string &result,
									bool interactive=true);
	static void addFinalSlash(std::string &directoryPath);
	static void removeFinalSlash(std::string &directoryPath);
	/*!
	 * List the content of a directory, limited to entries with a particular file extension.
	 *
	 * @param entries	     The vector in which relevant directory entries will be inserted
	 * @param srcDirectory   The directory to explore (absolute or relative path)
	 * @param extension      The file extension to consider (no wildcards, ignored if empty string)
	 * @param startString    The string to be matched by the first part of the file name (no wildcards,
	 * 						 ignored if empty string).
	 * @param regularFilesOnly If true, only regular (as defined by POSIX path_stat) files are reported
	 * 						   (no symlinks, no directories etc.)
	 * @param appendToEntries If true, the entries vector is not emptied before inserting the discovered
	 * 						  entries
	 * @return True if the directory was successfully listed, false otherwise.
	 */
	static bool listDirectory(		std::vector<std::string> &entries,
									const std::string &srcDirectory,
									const std::string &extension,
									const std::string &startString,
									bool regularFilesOnly=true,
									bool appendToEntries=false);

	/*!
	 * List the content of a directory, limited to entries with a particular file extension.
	 *
	 * @param entries	   The vector in which relevant directory entries will be inserted
	 * @param srcDirectory The directory to explore (absolute or relative path)
	 * @param extension    The file extension to consider (no wildcards, ignored if empty string)
	 * @param regularFilesOnly If true, only regular (as defined by POSIX path_stat) files are reported
	 * 						   (no symlinks, no directories etc.)
	 * @param appendToEntries If true, the entries vector is not emptied before inserting the discovered
	 * 						  entries
	 * @return True if the directory was successfully listed, false otherwise.
	 */
	static bool listDirectory(		std::vector<std::string> &entries,
									const std::string &srcDirectory,
									const std::string &extension,
									bool regularFilesOnly=true,
									bool appendToEntries=false);
	/** Create a directory.
	 *  @param dirPath The path to the directory to be created.
	 *  @param interactive If true, messages are sent to stderr in case of problem.
	 *  @param silentIfExists If true and the directory already exists, the function
	 *         				  will assume it was succesful. Otherwise it will return false.
	 *  @return True if successful, false otherwise.
	 */
	static bool createDirectory(	const std::string& dirPath,
									bool interactive=true,
									bool silentIfExists=true);
	/** Remove an empty directory.
	 *  @pre The directory is empty
	 *  @param dirPath The path to the directory to remove
	 *  @param silentIfInexistent If true and destination directory does not exist,
	 *  						  removal is considered successful.
	 *  						  Otherwise the operation fails.
	 *  @param interactive If true, feedback is provided on cout.
	 *  @return True if the directory was successfully removed, false otherwise.
	 */
	static bool removeDirectory(	const std::string& dirPath,
									bool interactive=true,
									bool silentIfInexistent=true);

	/** Extract path to the directory and the base (i.e. the name of the file) from
	 *  a relative or absolute file path.
	 *  @param path The path to analyze
	 *  @param dir The string to set to the path to the directory
	 *  @param base The string to set to the file name.
	 */
	static void relativeDirBaseSplit(	const std::string& path,
										std::string& dir,
										std::string& base);

	/** Build a filename as tttttt_lllllll.ext where ttttt is a human-readable timestamp,
	 *  lllllll is a label and ext is the extension.
	 *  @param label The label to use in the filename. If label is an empty std::string, the underscore
	 *  	   		 is not included in the file name.
	 *  @param extension The extension. Ditto for the dot.
	 *  @remark <a href="https://stackoverflow.com/questions/15777073/how-do-you-print-a-c11-time-point">Useful article</a>
	 */
	static std::string getTimestampedFileName(const std::string& label="", const std::string& extension="");


protected:
	static bool chdir_getcwd(const std::string& dir, std::string& path);
	static bool realpath_file(const std::string& path, std::string& resolved_path);
	static bool readlink_internal(const std::string& path, std::string& buffer, size_t length);
	static void build_path_base_swap(std::string &path, const std::string& newbase);
	static bool fileEntryExists(const std::string& path, mode_t &stmode );
	/** A cp implementation using normal POSIX calls and without any loop.
	 *  Code inspired from the buffer copy variant of the answer of caf.
	 *  Source: https://stackoverflow.com/questions/2180079/how-can-i-copy-a-file-on-unix-using-c
	 *  @Warning: Using mmap can easily fail on 32 bit systems, on 64 bit system the danger is less likely.
	 */
	static bool cp(const char *to, const char *from);

} ;
