//============================================================================
// Name        : RT-ProcessingMain.cpp
// Author      : CSPU: Cansat 2019 (IsaTwo)
// Copyright   : Collège Saint-Pierre, Uccle
// Description : The main function for the RT-Processing executable.
//============================================================================

#include <iostream>
#include <exception>

using namespace std;

#include "DebugUtils.h"
#include <RT_Processing.h>

INITIALIZE_EASYLOGGINGPP

int main(int argc, char* argv[]) {
	int resultCode;

	try {
		// Set up logging
		START_EASYLOGGINGPP(argc, argv);
		configureLogging();

		RT_Processing rtp;
		resultCode=rtp.run(argc, argv);
	}
	catch(exception &e) {
		cout << "Unexpected exception received in main" << endl;
		cout << e.what();
		resultCode = -99;
	}

	return resultCode;
}
