#include "TestImgRecordMapping.h"
#include "ImgRecordMapping.h"
#include "FileMgr.h"
#include "cute.h"

#include "DebugUtils.h"
//constexpr auto DBG=false;

#include <chrono>

using namespace std;

string testFilesDir("TestFiles/ImgRecordMapping");
string inexistentDir=testFilesDir+"/INEXISTENT_FOLDER";
string emptyDir=testFilesDir+"/emptyFolder";
string outputDir=testFilesDir+"/outputDir";
string nonImgFile=emptyDir+"/nonImgFile.txt";
string inputDir=testFilesDir+"/inputDir";
string fileWithInvalidName=inputDir+"/invalid.jpg";
string dataFolder1=testFilesDir+"/dataFile1_DoNotDelete";
string dataFolder2=testFilesDir+"/dataFile2_DoNotDelete";

// Install original file/folder context.
void initTestFiles() {
	FileMgr::createDirectory(testFilesDir, false,true);
	FileMgr::createDirectory(emptyDir, false,true);
	FileMgr::createDirectory(inputDir, false,true);
	if (FileMgr::directoryExists(outputDir)) {
		FileMgr::deleteFiles(outputDir,"*",false);
		FileMgr::removeDirectory(outputDir,false,true);
	}
	ASSERT(!FileMgr::directoryExists(outputDir));
	ASSERT(FileMgr::directoryExists(dataFolder1));
ASSERT(FileMgr::directoryExists(dataFolder2));
}

void clearTestFiles() {
	FileMgr::deleteFile(nonImgFile, true);
	FileMgr::deleteFile(fileWithInvalidName, true);
	if (FileMgr::directoryExists(outputDir)) {
		FileMgr::deleteFiles(outputDir,"*",false);
		FileMgr::removeDirectory(outputDir,false,true);
	}
}

void Test_DirectoryIssues() {
	initTestFiles();
	ImgRecordMapping* rm;
	// Fail because inexistent input dir.
	ASSERT_THROWS(rm=new ImgRecordMapping(inexistentDir, outputDir, 1000), runtime_error);
	FileMgr::removeDirectory(outputDir,false,true);
	// Fail because empty input dir
	ASSERT_THROWS(rm=new ImgRecordMapping(emptyDir, outputDir, 1000), runtime_error);
	FileMgr::removeDirectory(outputDir,false,true);
	// Create a non image file in input directory, then fail because no image file
	// in input directory
	if (!FileMgr::fileExists(nonImgFile))
	{
		fstream outfile;
		outfile.open(nonImgFile, ios::out);
		outfile << "This a file which is not an image file, and should be ignored" << endl;
		outfile.close();
	}
	ASSERT_THROWS(rm=new ImgRecordMapping(emptyDir, outputDir, 1000), runtime_error);

	// Fail because output directory exists
	if (!FileMgr::directoryExists(outputDir)) {
		FileMgr::createDirectory(outputDir);
	}
	ASSERT_THROWS(rm=new ImgRecordMapping(emptyDir, outputDir, 1000), runtime_error);
	FileMgr::removeDirectory(outputDir,false,true);
	clearTestFiles();
}

void Test_ErroneousFileNames() {
	initTestFiles();
	ImgRecordMapping* rm;
	if (!FileMgr::fileExists(fileWithInvalidName))
		{
			fstream outfile;
			outfile.open(fileWithInvalidName, ios::out);
			outfile << "Simulate an image file with a name which is not a timestamp" << endl;
			outfile.close();
		}
	cout << "File with invalid name should be ignored, and an exception should be thrown " << endl;
	cout << "because no file with valid names is found." << endl;
	ASSERT_THROWS(rm=new ImgRecordMapping(inputDir, outputDir, 1000), runtime_error);
	clearTestFiles();
}

void Test_FileMapping1() {
	initTestFiles();
	cout << "This test should map files but: " << endl;
	cout << "  - skip one because not record is lose enough" << endl;
	cout << "  - skip two files because they map to the same " << endl;
	cout << "    record as a previous one" << endl;
	cout << "  - leave last file unmapped because there is no record for it" << endl;
	ImgRecordMapping rm(dataFolder1,outputDir, 10000);
	IsaTwoGroundRecord record;
	record.clear();
	for (record.timestamp=35080; record.timestamp < 36000; record.timestamp += 80) {
		rm.processRecord(record);
	}
	ASSERT(FileMgr::fileExists(outputDir+"/00035080.jpg"));
	ASSERT(FileMgr::fileExists(outputDir+"/00035880.jpg"));
	ASSERT(FileMgr::fileExists(outputDir+"/00035160.jpg"));
	vector<string> entries;
	FileMgr::listDirectory(entries, outputDir, "jpg", true, false);
	ASSERT(entries.size()==3);
	clearTestFiles();
}

void Test_FileMapping2() {
	initTestFiles();
	cout << "This test should map files but: " << endl;
	cout << "  - skip one because not record is lose enough" << endl;
	cout << "  - skip two files because they map to the same " << endl;
	cout << "    record as a previous one" << endl;
	cout << "  - exhaust files to map before records are exhausted" << endl;
	ImgRecordMapping rm(dataFolder1,outputDir, 10000);
	IsaTwoGroundRecord record;
	record.clear();
	for (record.timestamp=35080; record.timestamp < 40000; record.timestamp += 80) {
		rm.processRecord(record);
	}
	ASSERT(FileMgr::fileExists(outputDir+"/00035080.jpg"));
	ASSERT(FileMgr::fileExists(outputDir+"/00035880.jpg"));
	ASSERT(FileMgr::fileExists(outputDir+"/00035160.jpg"));
	vector<string> entries;
	FileMgr::listDirectory(entries, outputDir, "jpg", true, false);
	ASSERT(entries.size()==4);
	clearTestFiles();
}
cute::suite make_suite_TestImgRecordMapping() {
	cute::suite s { };
	s.push_back(CUTE(Test_DirectoryIssues));
	s.push_back(CUTE(Test_ErroneousFileNames));
	s.push_back(CUTE(Test_FileMapping1));
	s.push_back(CUTE(Test_FileMapping2));

	return s;
}
